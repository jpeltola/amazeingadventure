A Mazeing Adventure is an Unreal Engine project made along with an online course of Ryan Laley Games in Youtube. The project is made using Unreal Engine Blueprint editor.

Engine version is 4.25.4

The point of this project is to learn some UE4 features and Blueprint. Also most of the static meshes are made by me, so I have also been learning using Blender and Unreal Engine Material editor.

There is a total of 5 mazes in the game.

Controls:
<ul>
    <li> WASD to move </li>
    <li> Shift to run </li>
    <li> Space to jump </li>
    <li> E to interact </li>
</ul>

